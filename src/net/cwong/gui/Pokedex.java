package net.cwong.gui;

import java.text.DecimalFormat;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import net.cwong.database.SQLConnector;

public class Pokedex extends Application {
	private GridPane pane;
	private Image image;
	private ImageView iv1;
	private HBox hbox;
	private HBox hbox2;
	private HBox hw;
	private HBox types;
	private HBox name;
	private Label type1, type2;
	private Label height, weight;

	private int pointer = 1;
	private String input;
	private Button previous, next, searchButton;
	private SQLConnector sqlc;
	private Label nameLabel;
	private TextField searchBar;
	private HBox hbox3;
	private String colorValue;

	@Override
	public void start(Stage primaryStage) throws Exception {
		previous = new Button();
		next = new Button();
		hbox = new HBox(10);
		hbox2 = new HBox(10);
		hbox3 = new HBox(10);
		hw = new HBox(10);
		types = new HBox(10);
		name = new HBox(10);
		pane = new GridPane();
		type1 = new Label();
		type2 = new Label();
		height = new Label();
		weight = new Label();
		nameLabel = new Label();
		searchButton = new Button();
		searchBar = new TextField();
		searchBar.setPromptText("Search...");

		previous.setId("button-round");
		previous.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				if (pointer > 1) {
					pointer--;
					hbox.getChildren().remove(iv1);
					sqlConnectDataNo();
					showPokeImage();
					showUpdatePokeData();
					checkAndUpdateColorTypes();
				}
			}

		});
		next.setId("button-round");
		next.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				if (pointer < 150) {
					pointer++;
					hbox.getChildren().remove(iv1);
					sqlConnectDataNo();
					showPokeImage();
					showUpdatePokeData();
					checkAndUpdateColorTypes();
				}

			}

		});

		searchButton.setId("button-search");
		searchButton.setOnAction(new EventHandler<ActionEvent>() {

			public void handle(ActionEvent event) {
				input = searchBar.getText();
				input = input.substring(0, 1).toUpperCase() + input.substring(1);
				input.trim();
				sqlConnectDataName();
				pointer = sqlc.getNumber();
				showUpdatePokeData();
				hbox.getChildren().remove(iv1);
				showPokeImage();
				checkAndUpdateColorTypes();

			}
		});

		hw.setAlignment(Pos.CENTER);
		types.setAlignment(Pos.CENTER);
		hbox.setAlignment(Pos.CENTER);
		hbox2.setAlignment(Pos.CENTER);
		hbox3.setAlignment(Pos.BOTTOM_RIGHT);
		name.setAlignment(Pos.CENTER);

		hbox2.getChildren().addAll(previous, next);
		hbox3.getChildren().addAll(searchButton);
		showPokeImage();
		sqlConnectDataNo();
		showUpdatePokeData();

		nameLabel.setId("name");
		name.getChildren().add(nameLabel);
		hw.getChildren().addAll(height, weight);

		checkAndUpdateColorTypes();

		types.getChildren().addAll(type1, type2);

		sqlc.getWeight();

		pane.setVgap(10);
		pane.setHgap(10);
		pane.setAlignment(Pos.TOP_CENTER);
		pane.setPadding(new Insets(40, 10, 10, 10));
		pane.add(hbox, 0, 0);
		pane.add(name, 0, 1);
		pane.add(hw, 0, 2);
		pane.add(types, 0, 3);
		pane.add(hbox2, 0, 20);
		pane.add(searchBar, 0, 10);
		pane.add(hbox3, 0, 11);

		Scene scene = new Scene(pane, 300, 500);
		scene.getStylesheets().add(Pokedex.class.getResource("style.css").toExternalForm());
		primaryStage.setScene(scene);
		primaryStage.setTitle("Pokedex");
		primaryStage.show();
	}

	private void checkAndUpdateColorTypes() {
		checkType(type1);
		checkType(type2);
	}

	private void checkType(Label type) {
		switch (type.getText()) {
		case "bug":
			colorValue = "#A6B91A";
			break;
		case "dragon":
			colorValue = "#6F35FC";
			break;
		case "electric":
			colorValue = "#F7D02C";
			break;
		case "fairy":
			colorValue = "#D685AD";
			break;
		case "fighting":
			colorValue = "#C22E28";
			break;
		case "fire":
			colorValue = "#EE8130";
			break;
		case "flying":
			colorValue = "#A98FF3";
			break;
		case "ghost":
			colorValue = "#735797";
			break;
		case "grass":
			colorValue = "#7AC74C";
			break;
		case "ground":
			colorValue = "#E2BF65";
			break;
		case "ice":
			colorValue = "#96D9D6";
			break;
		case "normal":
			colorValue = "#A8A77A";
			break;
		case "poison":
			colorValue = "#A33EA1";
		case "psychic":
			colorValue = "#F95587";
			break;
		case "steel":
			colorValue = "#B7B7CE";
			break;
		case "rock":
			colorValue = "#B6A136";
			break;
		case "water":
			colorValue = "#6390F0";
			break;
		default:
			colorValue = "red";
			break;
		}
		setColor(type, colorValue);
	}

	private void setColor(Label type, String value) {
		type.setStyle("-fx-background-color:" + value + ";" + "-fx-alignment:center;" + "-fx-max-width:100px;"
				+ "-fx-min-width:100px;");
	}

	public void showUpdatePokeData() {
		nameLabel.setText(sqlc.getName());
		height.setText(convertHeight(sqlc.getHeight()) + " ft");
		weight.setText(convertWeight(sqlc.getWeight()) + " lbs");
		type1.setText(sqlc.getType());
		type2.setText(sqlc.getType2());
	}

	// compared pokemon weight from api to confirmed weight (pokemon.com)
	// weight from api is pokemon weight in kilo multiplied by 10
	// assume the api weight is due to data transfer error
	private String convertWeight(double kiloTimesTen) {
		double divideByTen = kiloTimesTen / 10;
		double lbs = divideByTen * 2.2;

		return cleanDecimal(lbs);
	}

	// comparing data from api to confirmed height (pokemon.com)
	// height in feet is approximately data from api divided by 3
	private String convertHeight(double unknownHeight) {
		double heightinFt = unknownHeight / 3;

		return cleanDecimal(heightinFt);
	}

	private String cleanDecimal(double number) {
		DecimalFormat df = new DecimalFormat("0.00");
		df.setMaximumFractionDigits(1);
		return df.format(number);

	}

	private void sqlConnectDataNo() {
		sqlc = new SQLConnector();
		sqlc.connectAndRead(pointer);
	}

	private void sqlConnectDataName() {
		sqlc = new SQLConnector();
		sqlc.connectAndRead(input);
	}

	private void showPokeImage() {
		image = new Image("resources/" + pointer + ".png");
		iv1 = new ImageView();
		iv1.setImage(image);
		iv1.setFitWidth(100);
		iv1.setPreserveRatio(true);
		iv1.setSmooth(true);
		iv1.setCache(true);

		hbox.setId("poke-image");
		hbox.getChildren().add(iv1);
	}

	public static void main(String[] args) {
		launch(args);

	}

}
