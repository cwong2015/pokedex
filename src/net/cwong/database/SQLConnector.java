package net.cwong.database;

import java.sql.*;

public class SQLConnector {

	private int no;
	private String name;
	private String type1;
	private String type2;
	private int height;
	private int weight;
	private String SQLCommand;

	public SQLConnector() {

	}

	public void connectAndRead(String pointer) {
		SQLCommand = "SELECT * FROM poketable WHERE name = " + "'" + pointer + "'" + ";";
		byId(SQLCommand);
	}

	public void connectAndRead(int pointer) {
		SQLCommand = "SELECT * FROM poketable WHERE no = " + pointer + ";";
		byId(SQLCommand);
	}

	public void byId(String SQLCommand) {
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite::resource:resources/pokebase.sqlite");
			c.setAutoCommit(false);

			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery(SQLCommand);

			no = rs.getInt("no");
			name = rs.getString("name");
			type1 = rs.getString("type1");
			type2 = rs.getString("type2");
			height = rs.getInt("height");
			weight = rs.getInt("weight");

			rs.close();
			stmt.close();
			c.close();
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
	}

	public int getNumber() {
		return no;
	}

	public String getName() {
		return name;
	}

	public String getType() {
		return type1;
	}

	public String getType2() {
		if (type2.equals("(None)")) {
			return "";
		}
		return type2;
	}

	public double getHeight() {
		return height;
	}

	public double getWeight() {
		return weight;
	}
}